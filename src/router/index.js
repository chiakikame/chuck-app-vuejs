import Vue from 'vue';
import Router from 'vue-router';
import Joke from 'components/Joke';
import About from 'components/About';
import NotFound from 'components/NotFound';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Joke,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
    },
    {
      path: '*',
      redirect: '/404',
    },
  ],
});
