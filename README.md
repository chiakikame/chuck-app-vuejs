# chuck-app

An application which displays jokes via https://api.chucknorris.io/ using its API.

## Asset credits

Nuclear_symbol.svg:
https://commons.wikimedia.org/wiki/File:Nuclear_symbol.svg

default.svg (by East718):
https://commons.wikimedia.org/wiki/File:718smiley.svg

error.svg (by Fabián Alexis):
Grabbed from https://commons.wikimedia.org/wiki/File:Antu_dialog-error.svg
Source: https://github.com/fabianalexisinostroza/Antu

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how everything work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
